import gc
import h5py
import math
import time
import pickle
import numpy as np
import pandas as pd
from tqdm import tqdm
import skimage.io
import skimage.transform
import matplotlib.pyplot as plt
import scipy.ndimage
import multiprocessing as mp

def read_im_train(f,tags,labels,plot):
    x_train = []
    y_train = []
    img = skimage.io.imread('input/train-jpg/{}.jpg'.format(f))
    img = img/255.0
    if(img.shape[0]>npix):
        img = skimage.transform.resize(img, (npix, npix),mode='constant')

    targets = np.zeros(17)
    for t in tags.split(' '):
        targets[label_map[t]] = 1

    x_train.append(img)
    y_train.append(targets)

    return x_train,y_train

def read_im_test(f):
    x_test = []
    img = skimage.io.imread('input/test-jpg/{}.jpg'.format(f))
    img = img/255.0
    if(img.shape[0]>npix):
        img = skimage.transform.resize(img, (npix, npix),mode='constant')

    x_test.append(img)

    return x_test

# Parameters
npix            = 192
ncpu            = 128
split           = 0.20
seed_sss        = 777
nfolds          = 10
labels_name     = 'input/labels_px'+str(npix)+'_jpg.p'
train_data_name = 'input/train_px'+str(npix)+'_jpg.h5'
test_data_name  = 'input/test_px'+str(npix)+'_jpg.h5'
df_train        = pd.read_csv('input/train_v2.csv')
df_test         = pd.read_csv('input/sample_submission_v2.csv')
write_train     = True
write_test      = True
plot            = False

x_train = []
x_test  = []
y_train = []

print('________________________________________')
print('labels     filename :',labels_name)
print('train data filename :',train_data_name)
print('________________________________________')


flatten = lambda l: [item for sublist in l for item in sublist]
labels  = sorted(list(set(flatten([l.split(' ') for l in df_train['tags'].values]))))

order = [5,6,10,11,0,1,2,3,4,7,8,9,12,13,14,15,16]
labels = np.array(labels)[order]
print(labels)

label_map     = {l: i for i, l in enumerate(labels)}
inv_label_map = {i: l for l, i in label_map.items()}

with open(labels_name,'wb') as p:
       pickle.dump(labels,p)

if(write_train):
    print('Reading train data')
    pool = mp.Pool(processes=ncpu)
    results = []
    for f, tags in df_train.values:
        results.append(pool.apply_async(read_im_train,[f,tags,labels,plot]))
    
    with tqdm(total=len(results)) as pbar:
        inc_count_old = len(results)
        while True:
            inc_count = sum(1 for x in results if not x.ready())
            pbar.update(inc_count_old-inc_count)
            if inc_count == 0:
                break
            inc_count_old = inc_count
            time.sleep(.1)
    
    for r in results:
        x,y = r.get()
        x_train.extend(x)
        y_train.extend(y)
    pool.close()
    pool.join()
    
    y_train = np.array(y_train, np.uint8)
    x_train = np.array(x_train, np.float16)
    if(len(x_train.shape)<4):
        x_train = np.expand_dims(x_train,axis=3)
    print('x_train shape = ',x_train.shape)
    print('y_train shape = ',y_train.shape)

    with h5py.File(train_data_name, 'w') as hf:
        hf.create_dataset("x_train", data=x_train)
        hf.create_dataset("y_train", data=y_train)
    
    del x_train,y_train
    gc.collect()

if(write_test):
    print('Reading test data')
    pool = mp.Pool(processes=ncpu)
    results = []
    for f, tags in df_test.values:
        results.append(pool.apply_async(read_im_test,[f]))
    
    with tqdm(total=len(results)) as pbar:
        inc_count_old = len(results)
        while True:
            inc_count = sum(1 for x in results if not x.ready())
            pbar.update(inc_count_old-inc_count)
            if inc_count == 0:
                break
            inc_count_old = inc_count
            time.sleep(.1)
    
    for r in results:
        x = r.get()
        x_test.extend(x)
    pool.close()
    pool.join()
    
    x_test = np.array(x_test, np.float16)
    if(len(x_test.shape)<4):
        x_test = np.expand_dims(x_test,axis=3)
    print('x_test shape = ',x_test.shape)
    
    with h5py.File(test_data_name, 'w') as hf:
        hf.create_dataset("x_test", data=x_test)


