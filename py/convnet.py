import os
import gc
import sys
import math
import h5py
import time
import json
import pickle
import operator
import configparser
import numpy as np
import pandas as pd
import keras as k
from keras import backend as K
import matplotlib.pyplot as plt
from tqdm import tqdm
import sklearn.model_selection as model_selection
from sklearn.metrics import fbeta_score
from keras.preprocessing.image import ImageDataGenerator

class config_obj():
    """ Object containing the configuration of the neural network """

    def parse(self, config_file):
        """ Configuration file parser """
        config = configparser.ConfigParser()
        config.read(config_file)

        try:
            self.npix = config.getint('convnet', 'npix')
        except:
            self.npix = 64
        try:
            self.nbatch = config.getint('convnet', 'nbatch')
        except:
            self.nbatch = 512
        try:
            self.std_noise = config.getfloat('convnet', 'std_noise')
        except:
            self.std_noise = 0.0
        try:
            self.split = config.getfloat('convnet', 'split')
        except:
            self.split = 0.20
        try:
            self.ncv = config.getint('convnet', 'ncv')
        except:
            self.ncv = 10
        try:
            self.drop = config.getfloat('convnet', 'drop')
        except:
            self.drop = 0.25
        try:
            self.conv_filters = json.loads(
                config.get('convnet', 'conv_filters'))
        except:
            self.conv_filters = [32, 64, 128, 256]
        try:
            self.fsx_row = json.loads(config.get('convnet', 'fsx_row'))
        except:
            self.fsx_row = [3]*len(self.conv_filters)
        try:
            self.fsy_row = json.loads(config.get('convnet', 'fsy_row'))
        except:
            self.fsy_row = self.fsx_row
        try:
            self.conv_row = json.loads(config.get('convnet', 'conv_row'))
        except:
            self.conv_row = [2]*len(self.conv_filters)
        try:
            self.pool_row = json.loads(config.get('convnet', 'pool_row'))
        except:
            self.pool_row = [2]*len(self.conv_filters)
        try:
            self.pool_method = json.loads(config.get('convnet', 'pool_method'))
        except:
            self.pool_method = ["max"]*len(self.conv_filters)
        try:
            self.neurons = json.loads(config.get('convnet', 'neurons'))
        except:
            self.neurons = []
        try:
            self.epochs = json.loads(config.get('convnet', 'epochs'))
        except:
            self.epochs = [20,5,5]
        try:
            self.lr = json.loads(config.get('convnet', 'lr'))
        except:
            self.lr = [1e-3,5e-4,2.5e-4]
        try:
            self.model_path = config.get('convnet', 'model_path')
        except:
            self.model_path = 'model/npix' + str(self.npix) +'.h5'
        try:
            self.seed_cv = config.getint('convnet', 'seed_cv')
        except:
            self.seed_cv = 777
        try:
            self.fs_flip = config.getboolean('convnet', 'fs_flip')
        except:
            self.fs_flip = False
        try:
            self.plot = config.getboolean('convnet', 'plot')
        except:
            self.plot = False
        try:
            self.unet = config.getboolean('convnet', 'unet')
        except:
            self.unet = False
        try:
            self.stratified = config.getboolean('convnet', 'stratified')
        except:
            self.stratified = False
        try:
            self.restart_weights = config.getboolean(
                'convnet', 'restart_weights')
        except:
            self.restart_weights = False
        try:
            self.overwrite = config.getboolean('convnet', 'overwrite')
        except:
            self.overwrite = False
        try:
            self.opt = config.get('convnet', 'opt')
        except:
            self.opt = 'Adam'
        try:
            self.train_data = config.get('convnet', 'train_data')
        except:
            self.train_data = 'input/train_px' + str(self.npix) + '.h5'
        try:
            self.test_data = config.get('convnet', 'test_data')
        except:
            self.test_data = 'input/test_px' + str(self.npix) + '.h5'
        try:
            self.labels = config.get('convnet', 'labels')
        except:
            self.labels = 'input/labels_px' + str(self.npix) + '.p'
        try:
            self.submission_name = config.get('convnet', 'submission_name')
        except:
            self.submission_name = 'submissions/submission_jpg_px' + \
                str(self.npix) + '.csv'
        try:
            self.thres_input = config.get('convnet','thres_input')
        except:
            self.thres_input = 'p_valid'
        try:
            self.f2_res = config.getint('convnet','f2_res')
        except:
            self.f2_res = 300
        try:
            self.train_naugment = config.getint('convnet','train_naugment')
        except:
            self.train_naugment = 3
        try:
            self.valid_naugment = config.getint('convnet','valid_naugment')
        except:
            self.valid_naugment = 3
        try:
            self.test_naugment = config.getint('convnet','test_naugment')
        except:
            self.test_naugment = 1
        try:
            self.use_folds = np.array(json.loads(config.get('convnet','use_folds')))
            self.use_folds -= 1
        except:
            self.use_folds = np.arange(conf.ncv).astype(int)
        try:
            self.zca = config.getboolean('convnet','zca')
        except:
            self.zca = False
        try:
            self.split_output = config.getboolean('convnet','split_output')
        except:
            self.split_output = False
        try:
            self.vertical_flip = config.getboolean('convnet','vertical_flip')
        except:
            self.vertical_flip = True
        try:
            self.horizontal_flip = config.getboolean('convnet','horizontal_flip')
        except:
            self.horizontal_flip = True
        try:
            self.rotation_range = config.getfloat('convnet','rotation_range')
        except:
            self.rotation_range = 0.0
        try:
            self.zoom_range = config.getfloat('convnet','zoom_range')
        except:
            self.zoom_range = 0.0
        try:
            self.batchnorm = config.getboolean('convnet','batchnorm')
        except:
            self.batchnorm = False
        try:
            self.net = config.get('convnet','net')
        except:
            self.net = ''

def conv_single(layer_in, fsx, fsy, fs_flip, nfilter, nsconv, pool, poolm, drop, batchnorm):

    branch = k.layers.Conv2D(nfilter, kernel_size=(
        fsx, fsy), activation='relu', padding='same')(layer_in)
    fsx2 = fsx
    fsy2 = fsy
    for i in range(nsconv - 1):
        if(fs_flip):
            fsx2_save = fsx2
            fsx2 = fsy2
            fsy2 = fsx2_save
        branch = k.layers.Conv2D(nfilter, kernel_size=(fsx2, fsy2),
                                 activation='relu', padding='same')(branch)
    if(batchnorm):
        branch = k.layers.BatchNormalization()(branch)
    if(pool > 1):
        if(poolm=='max'):
            branch = k.layers.MaxPooling2D(pool_size=(
                pool, pool), strides=(pool, pool))(branch)
        elif(poolm=='average'):
            branch = k.layers.AveragePooling2D(pool_size=(
                pool, pool), strides=(pool, pool))(branch)
        else:
            branch = k.layers.MaxPooling2D(pool_size=(
                pool, pool), strides=(pool, pool))(branch)
    branch = k.layers.Dropout(drop)(branch)
    return branch


def unode(layer_in1, layer_in2, fs, nfilter, nsconv, pool, drop):
    up = k.layers.concatenate([k.layers.Conv2DTranspose(nfilter, (pool, pool), strides=(
        pool, pool), padding='same')(layer_in1), layer_in2], axis=3)
    conv = k.layers.Conv2D(
        nfilter, (fs, fs), activation='relu', padding='same')(up)
    for i in range(nsconv - 1):
        conv = k.layers.Conv2D(
            nfilter, (fs, fs), activation='relu', padding='same')(conv)
    out = k.layers.Dropout(drop)(conv)
    return out


def conv_cascade(layer_in, conf):
    branches = []
    branches.append(layer_in)
    for i in range(len(conf.conv_filters)):
        if((i == len(conf.conv_filters)) and (conf.unet)):
            pool = 1
        else:
            pool = conf.pool_row[i]
        branches.append(conv_single(
            branches[-1], conf.fsx_row[i], conf.fsy_row[i], conf.fs_flip,
            conf.conv_filters[i], conf.conv_row[i], pool, conf.pool_method[i],
            conf.drop,conf.batchnorm))

    for i in range(len(conf.fsx_row)):
        if(conf.fsy_row[i]!=conf.fsy_row[i]):
            conf.unet = False
    if(conf.unet):
        bottom = len(branches)-1
        for i in range(1,len(conf.conv_filters)):
            branches.append(unode(branches[-1], branches[bottom-i], conf.fsx_row[i],
                                  conf.conv_filters[i], conf.conv_row[i], conf.pool_row[i], conf.drop))
        branches.append(k.layers.Conv2D(
            1, (1, 1), activation='sigmoid')(branches[-1]))


    return branches[-1]


def create_cnn(x_train,myopt,conf):
    if(conf.net==''):
        inp = k.layers.Input(shape=x_train[0, :, :, :].shape, name='input1')
        norm1 = k.layers.BatchNormalization()(inp)
        norm1 = k.layers.noise.GaussianNoise(conf.std_noise)(norm1)
        branch1 = conv_cascade(norm1, conf)
    else:
        mynet = operator.attrgetter(conf.net)(k.applications)
        model = mynet(input_shape=x_train.shape[1:],include_top=False,pooling='max')
        branch1 = model.layers[-1].output
        inp = model.layers[0].input

    # Flatten
    branch1 = k.layers.Flatten()(branch1)
    # Hidden layers
    for i in range(len(conf.neurons)):
        branch1 = k.layers.Dense(conf.neurons[i], activation='relu')(branch1)
        if(i<len(conf.neurons)-1):
            branch1 = k.layers.Dropout(conf.drop)(branch1)
    # Batch norm before classification layer
    branch1 = k.layers.BatchNormalization()(branch1)
    branch1 = k.layers.Dropout(conf.drop * 2)(branch1)
    # Classification output
    if(conf.split_output):
        out1 = k.layers.Dense(4, activation='softmax')(branch1)
        out2 = k.layers.Dense(13, activation='sigmoid')(branch1)
        out = k.layers.concatenate(inputs=[out1, out2])
    else:
        out = k.layers.Dense(17,activation='sigmoid')(branch1)

    model = k.models.Model(inputs=inp, outputs=out)
    model.compile(loss='binary_crossentropy',
                  optimizer=myopt(), metrics=['accuracy'])
    return model


def mask_classes(p,x,xw,xc):
    p2 = np.zeros_like(p)
    # Per class threshold
    for i in range(17):
        p2[:, i] = (p[:, i] > x[i]).astype(np.int)
    # Weather exclusivity
    for i in range(4):
        maskw = p[:, i] > xw[i]
        p2[maskw, i] = 1
        for ii in range(4):
            if(ii!=i):
                p2[maskw, ii] = 0
    # Cloudy exclusivity
    maskc = p[:, 1] > xc
    p2[maskc, 1] = 1
    for ii in range(17):
        if(ii!=1):
            p2[maskc, ii] = 0

    return p2

def mask_score(y,p,x,xw,xc):
    p2 = mask_classes(p,x,xw,xc)
    score = f2_score(y, p2)
    return score

def optimise_f2_global_threshold(y, p, resolution=300):
    x = [0.2] * 17
    xw = [2.0] * 4
    xc = 2.0
    # Step 1 : global opt
    best_i2 = 0
    best_score = 0
    for i2 in range(resolution):
        i2 /= resolution
        x[:] = [i2] * 17
        score = mask_score(y,p,x,xw,xc)
        if score > best_score:
            best_i2 = i2
            best_score = score
    x[:] = [best_i2] * 17

    return best_i2

def optimise_f2_thresholds(y, p, labels, verbose=True, resolution=300):
    x = [0.2] * 17
    xw = [2.0] * 4
    xc = 2.0
    # Step 1 : global opt
    best_i2 = 0
    best_score = 0
    for i2 in range(resolution):
        i2 /= resolution
        x[:] = [i2] * 17
        score = mask_score(y,p,x,xw,xc)
        if score > best_score:
            best_i2 = i2
            best_score = score
    x[:] = [best_i2] * 17
    if verbose:
        print('_'*100)
        print('Global threshold={0:7.3f} f2={1:8.4f}'.format(
            best_i2, best_score))
        sys.stdout.flush()

    # Step 2 : per class opt
    if verbose:
        print('_'*100)
        print('Classes thresholds')
        print('_'*100)
    for i in range(17):
        best_i2 = 0
        best_score = 0
        for i2 in range(resolution):
            i2 /= resolution
            x[i] = i2
            score = mask_score(y,p,x,xw,xc)
            if score > best_score:
                best_i2 = i2
                best_score = score
        x[i] = best_i2
        if verbose:
            print('{0:3d}={1:20s} threshold={2:7.3f} f2={3:9.5f}'.format(
                i+1, labels[i], best_i2, best_score))
            sys.stdout.flush()

    # Step 3 : weather exclusivity
    if verbose:
        print('_'*100)
        print('Weather exclusivity')
        print('_'*100)
    for i in range(4):
        best_i2 = 0
        best_score = 0
        for i2 in range(resolution):
            i2 /= resolution
            xw[i] = i2
            score = mask_score(y,p,x,xw,xc)
            if score > best_score:
                best_i2 = i2
                best_score = score
        xw[i] = best_i2
        if verbose:
            print('{0:3d}={1:20s} threshold={2:7.3f} f2={3:9.5f}'.format(
                i+1, labels[i], best_i2, best_score))
            sys.stdout.flush()

    # Step 4 : cloudy exclusivity
    if verbose:
        print('_'*100)
        print('Cloudy exclusivity')
        print('_'*100)
    i = 1
    best_i2 = 0
    best_score = 0
    for i2 in range(resolution):
        i2 /= resolution
        xc = i2
        score = mask_score(y,p,x,xw,xc)
        if score > best_score:
            best_i2 = i2
            best_score = score
    xc = best_i2
    if verbose:
        print('{0:3d}={1:20s} threshold={2:7.3f} f2={3:9.5f}'.format(
            i+1, labels[i], best_i2, best_score))
        sys.stdout.flush()

    # Step 5 : last pass
    if verbose:
        print('_'*100)
        print('Classes thresholds')
        print('_'*100)
    for i in range(17):
        best_i2 = 0
        best_score = 0
        for i2 in range(resolution):
            i2 /= resolution
            x[i] = i2
            score = mask_score(y,p,x,xw,xc)
            if score > best_score:
                best_i2 = i2
                best_score = score
        x[i] = best_i2
        if verbose:
            print('{0:3d}={1:20s} threshold={2:7.3f} f2={3:9.5f}'.format(
                i+1, labels[i], best_i2, best_score))
            sys.stdout.flush()

    return x,xw,xc


def f2_score(y_true, y_pred):
    return fbeta_score(np.array(y_true), np.array(y_pred), beta=2, average='samples')


class f2_callback(k.callbacks.Callback):
    def __init__(self, x_val, y_val, batch_size=128, verbose=0):
        super(k.callbacks.Callback, self).__init__()
        self.batch_size = batch_size
        self.verbose = verbose
        self.x_val = x_val
        self.y_val = y_val

    def on_epoch_end(self, batch, logs={}):
        predict = np.asarray(self.model.predict(
            self.x_val, batch_size=self.batch_size))
        thres = optimise_f2_global_threshold(self.y_val, predict, resolution=100)
        print(' ---> f2_val = {0:.5f} (thres={1:.3f})'.format(f2_score(self.y_val, predict > thres),thres))
        sys.stdout.flush()
        return

def step_decay(epoch,initial_lr,drop, epochs_drop):
    lrate = initial_lr * math.pow(drop, math.floor((1+epoch)/epochs_drop))
    return lrate

def main():
    conf = config_obj()
    try:
        conf.parse(sys.argv[1])
    except:
        print('Could not read config file')
        sys.exit()
    print('Configuration')
    print('_'*100)
    for key, value in vars(conf).items():
        print('{0:20s} {1}'.format(key, value))
    print('_'*100)

    np.random.seed(conf.seed_cv)
    myopt = operator.attrgetter(conf.opt)(k.optimizers)

    with open(conf.labels, 'rb') as p:
        labels = pickle.load(p)

    with h5py.File(conf.train_data, 'r') as hf:
        x_train = hf['x_train'][:]
        y_train = hf['y_train'][:]

    datagen = ImageDataGenerator(
        horizontal_flip=conf.horizontal_flip,
        vertical_flip=conf.vertical_flip,
        rotation_range=conf.rotation_range,
        fill_mode='reflect',
        zoom_range=conf.zoom_range)
    datagen_test = ImageDataGenerator(
        horizontal_flip=conf.horizontal_flip,
        vertical_flip=conf.vertical_flip,
        rotation_range=conf.rotation_range,
        fill_mode='reflect')
    #print('Fitting data generator')
    #datagen.fit(x_train)

    trn_index = [[]]*conf.ncv
    val_index = [[]]*conf.ncv
    # change split value for getting different validation splits
    if(conf.stratified):
        print('Computing CV stratified KFolds')
        index = np.arange(x_train.shape[0])
        for i in tqdm(range(0, 17)):
            cv = model_selection.StratifiedShuffleSplit(
                n_splits=conf.ncv, test_size=conf.split, random_state=conf.seed_cv)
            j = 0
            for train_index, test_index in cv.split(index, y_train[:, i]):
                X_train, X_test = index[train_index], index[test_index]
                # to ensure there is no repetetion within each split and between the splits
                trn_index[j] = trn_index[j] + list(set(list(X_train)) - set(trn_index[j]) - set(val_index[j]))
                val_index[j] = val_index[j] + list(set(list(X_test)) - set(val_index[j]) - set(trn_index[j]))
                j += 1
    else:
        print('Computing CV KFolds')
        #cv = model_selection.ShuffleSplit(n_splits=conf.ncv, test_size=conf.split, random_state=conf.seed_cv)
        cv = model_selection.KFold(n_splits=conf.ncv, shuffle=True, random_state=conf.seed_cv)
        j = 0
        for train_index, test_index in cv.split(x_train, y_train):
            trn_index[j] = train_index
            val_index[j] = test_index
            j += 1

    labels = np.array(labels)
    t1_all = []
    t2_all = []
    t3_all = []
    print('_'*100)
    print('x_train shape = ', x_train.shape)
    print('y_train shape = ', y_train.shape)

    p_valid = []
    weights = []

    for fold in range(conf.ncv):
        if fold in conf.use_folds:
            model_path = conf.model_path+'_fold'+str(fold+1)+'_'+str(conf.ncv)+'.h5'
            print('model=',model_path)
            print('Fold {0}'.format(fold+1))
            print('_'*100)
            X_train = x_train[trn_index[fold]]
            X_valid = x_train[val_index[fold]]
            Y_train = y_train[trn_index[fold]]
            Y_valid = y_train[val_index[fold]]
            print('x_train shape = ',X_train.shape)
            print('y_train shape = ',Y_train.shape)
            print('x_valid shape = ',X_valid.shape)
            print('y_valid shape = ',Y_valid.shape)

            model = create_cnn(X_train,myopt,conf)
            if(conf.restart_weights):
                if(os.path.isfile(model_path)):
                    try:
                        print('_'*100)
                        print('Loading ', model_path)
                        print('_'*100)
                        model.load_weights(model_path)
                    except:
                        pass
            fit = False
            # Fit if there is no model
            if(not os.path.isfile(model_path)):
                fit = True
            # Fit if user requires overwriting
            if(conf.overwrite):
                fit = True
            # Fit if user wants "transfer learning"
            if(conf.restart_weights and os.path.isfile(model_path)):
                fit = True
            if(fit):
                model.summary()
                # Preparing callbacks
                callbacks = []
                callbacks.append(k.callbacks.ModelCheckpoint(
                    model_path, monitor='val_loss', save_best_only=True, verbose=0))
                callbacks.append(f2_callback(X_valid,Y_valid,batch_size=conf.nbatch, verbose=1))
                # Steps per epoch for fit_generator
                spe1 = int(math.ceil((X_train.shape[0]*conf.train_naugment)/float(conf.nbatch)))
                spe2 = int(math.ceil((X_valid.shape[0]*conf.valid_naugment)/float(conf.nbatch)))

                for ep,lr in zip(conf.epochs,conf.lr):
                    # Update learning rate
                    K.set_value(model.optimizer.lr, lr)
                    print('learning rate = {0:.2e}'.format(lr))
                    # Fit model using imagedata generator
                    model.fit_generator(datagen.flow(X_train,Y_train,batch_size=conf.nbatch,shuffle=True),
                            validation_data=datagen.flow(X_valid,Y_valid,batch_size=conf.nbatch,shuffle=True),
                            steps_per_epoch=spe1, validation_steps=spe2, verbose=1, epochs=ep,callbacks=callbacks)

                    # Load best val_loss model
                    model.load_weights(model_path)
            # Load best val_loss model
            model.load_weights(model_path)

            # Predict validation dataset to adjust thresholds
            p_valid = []
            for i in range(conf.valid_naugment):
                print('_'*100)
                print('Round {0} validation'.format(i+1))
                pvalid = np.zeros(Y_valid.shape)
                for j in tqdm(range(len(X_valid))):
                    im = datagen_test.random_transform(X_valid[j].astype(float))[np.newaxis]
                    pvalid[j] = model.predict(im, verbose=0)
                p_valid.append(pvalid)
                del pvalid
                gc.collect()
                t1,t2,t3 = optimise_f2_thresholds(Y_valid,p_valid[-1],labels,resolution=conf.f2_res,verbose=False)
                p_valid_masked = mask_classes(p_valid[-1],t1,t2,t3)
                f2 = f2_score(Y_valid, p_valid_masked)
                weights.append(f2)
                print('f2_val   = {0:.4f} '.format(f2))
                print('_'*100)
                del p_valid_masked
                gc.collect()
    p_valid_all = np.stack(p_valid,axis=len(p_valid[0].shape))
    del p_valid
    gc.collect()
    p_valid = np.average(p_valid_all,weights=weights,axis=len(p_valid_all.shape)-1)
    t1,t2,t3 = optimise_f2_thresholds(Y_valid,p_valid,labels,resolution=conf.f2_res,verbose=False)
    p_valid_masked = mask_classes(p_valid,t1,t2,t3)
    t1_all.append(t1)
    t2_all.append(t2)
    t3_all.append(t3)
    print('f2_val av = {0:.4f} '.format(f2_score(Y_valid, p_valid_masked)))
    del model
    gc.collect()
    print('_'*100)
    nvalid = float(len(Y_valid[:, 0]))
    for i in range(p_valid.shape[1]):
        y_column = Y_valid[:, i]
        p_column = p_valid_masked[:, i]
        fail = np.abs(y_column - p_column)
        y_npos = len(y_column[y_column == 1])
        p_npos = len(p_column[p_column == 1])
        print('{0:20s} -> fail rate = {1:7.2f}% | nfail = {2:5d} | y_npos = {3:5d} | p_npos = {4:5d})'.format(
            labels[i], 100. * np.sum(fail) / nvalid, len(fail[fail == 1]), y_npos, p_npos))
    print('_'*100)

    try:
        del X_valid, Y_valid
        gc.collect()
    except:
        pass

    print('Loading test data')
    p_test = []
    with h5py.File(conf.test_data, 'r') as hf:
        x_test = hf['x_test'][:]
        for fold in range(conf.ncv):
            if fold in conf.use_folds:
                model_path = conf.model_path+'_fold'+str(fold+1)+'_'+str(conf.ncv)+'.h5'
                print('Loading ', model_path)
                model = k.models.load_model(model_path)
                print('Predicting test')
                for i in range(conf.test_naugment):
                    ptest = np.zeros((x_test.shape[0],17))
                    for j in tqdm(range(len(x_test))):
                        im = datagen_test.random_transform(x_test[j].astype(float))[np.newaxis]
                        ptest[j] = model.predict(im, verbose=0)
                    p_test.append(ptest)
                    del ptest
                    gc.collect()
                del model
                gc.collect()
        print('')

    p_test_all = np.stack(p_test,axis=len(p_test[0].shape))
    del p_test
    gc.collect()
    print(p_test_all.shape)
    p_test = np.average(p_test_all,weights=weights,axis=len(p_test_all.shape)-1)
    del p_test_all
    gc.collect()
    print('Masking')
    t1_av = np.mean(np.array(t1_all),axis=0)
    t2_av = np.mean(np.array(t2_all),axis=0)
    t3_av = np.mean(np.array(t3_all))
    p_test_masked = mask_classes(p_test, t1_av, t2_av, t3_av)

    preds = []
    for i in tqdm(range(p_test_masked.shape[0]), miniters=1000):
        preds.append('')
        try:
            preds[-1] += ' '.join(labels[p_test_masked[i, :] == 1])
        except:
            pass

    df_test = pd.read_csv('input/sample_submission_v2.csv')
    df_test['tags'] = preds
    df_test.to_csv(conf.submission_name, index=False)


if __name__ == '__main__':
    main()
