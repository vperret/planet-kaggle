print('Starting model ensembling')
import os
import gc
import sys
import h5py
import time
import json
import pickle
import operator
import warnings
import configparser
import numpy as np
import pandas as pd
import keras as k
import matplotlib.pyplot as plt
from tqdm import tqdm
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.metrics import fbeta_score

def mask_classes(p,x,xw,xc):
    p2 = np.zeros_like(p)
    # Per class threshold
    for i in range(17):
        p2[:, i] = (p[:, i] > x[i]).astype(np.int)
    # Weather exclusivity
    for i in range(4):
        maskw = p[:, i] > xw[i]
        p2[maskw, i] = 1
        for ii in range(4):
            if(ii!=i):
                p2[maskw, ii] = 0
    # Cloudy exclusivity
    maskc = p[:, 1] > xc
    p2[maskc, 1] = 1
    for ii in range(17):
        if(ii!=1):
            p2[maskc, ii] = 0

    return p2

def mask_score(y,p,x,xw,xc):
    p2 = mask_classes(p,x,xw,xc)
    score = f2_score(y, p2)
    return score

def optimise_f2_global_threshold(y, p, resolution=300):
    x = [0.2] * 17
    xw = [2.0] * 4
    xc = 2.0
    # Step 1 : global opt
    best_i2 = 0
    best_score = 0
    for i2 in range(resolution):
        i2 /= resolution
        x[:] = [i2] * 17
        score = mask_score(y,p,x,xw,xc)
        if score > best_score:
            best_i2 = i2
            best_score = score
    x[:] = [best_i2] * 17

    return best_i2

def optimise_f2_thresholds(y, p, verbose=True, resolution=300):
    x = [0.2] * 17
    xw = [2.0] * 4
    xc = 2.0
    # Step 1 : global opt
    best_i2 = 0
    best_score = 0
    for i2 in range(resolution):
        i2 /= resolution
        x[:] = [i2] * 17
        score = mask_score(y,p,x,xw,xc)
        if score > best_score:
            best_i2 = i2
            best_score = score
    x[:] = [best_i2] * 17
    if verbose:
        print('threshold={0:7.3f} f2={1:8.4f} global'.format(
            best_i2, best_score))
        sys.stdout.flush()

    # Step 2 : per class opt
    print('_'*100)
    print('Classes thresholds')
    print('_'*100)
    for i in range(17):
        best_i2 = 0
        best_score = 0
        for i2 in range(resolution):
            i2 /= resolution
            x[i] = i2
            score = mask_score(y,p,x,xw,xc)
            if score > best_score:
                best_i2 = i2
                best_score = score
        x[i] = best_i2
        if verbose:
            print('{0:3d} threshold={1:7.3f} f2={2:9.5f}'.format(
                i+1, best_i2, best_score))
            sys.stdout.flush()

    # Step 3 : weather exclusivity
    print('_'*100)
    print('Weather exclusivity')
    print('_'*100)
    for i in range(4):
        best_i2 = 0
        best_score = 0
        for i2 in range(resolution):
            i2 /= resolution
            xw[i] = i2
            score = mask_score(y,p,x,xw,xc)
            if score > best_score:
                best_i2 = i2
                best_score = score
        xw[i] = best_i2
        if verbose:
            print('{0:3d} threshold={1:7.3f} f2={2:9.5f}'.format(
                i+1, best_i2, best_score))
            sys.stdout.flush()

    # Step 4 : cloudy exclusivity
    print('_'*100)
    print('Cloudy exclusivity')
    print('_'*100)
    i = 1
    best_i2 = 0
    best_score = 0
    for i2 in range(resolution):
        i2 /= resolution
        xc = i2
        score = mask_score(y,p,x,xw,xc)
        if score > best_score:
            best_i2 = i2
            best_score = score
    xc = best_i2
    if verbose:
        print('{0:3d}threshold={1:7.3f} f2={2:9.5f}'.format(
            i+1, best_i2, best_score))
        sys.stdout.flush()

    # Step 5 : last pass
    print('_'*100)
    print('Classes thresholds')
    print('_'*100)
    for i in range(17):
        best_i2 = 0
        best_score = 0
        for i2 in range(resolution):
            i2 /= resolution
            x[i] = i2
            score = mask_score(y,p,x,xw,xc)
            if score > best_score:
                best_i2 = i2
                best_score = score
        x[i] = best_i2
        if verbose:
            print('{0:3d} threshold={1:7.3f} f2={2:9.5f}'.format(
                i+1, best_i2, best_score))
            sys.stdout.flush()

    return x,xw,xc

def f2_score(y_true, y_pred):
    return fbeta_score(np.array(y_true), np.array(y_pred), beta=2, average='samples')


class f2_callback(k.callbacks.Callback):
    def __init__(self, batch_size=128, verbose=0):
        super(k.callbacks.Callback, self).__init__()
        self.batch_size = batch_size
        self.verbose = verbose

    def on_epoch_end(self, batch, logs={}):
        predict = np.asarray(self.model.predict(
            self.validation_data[:-3], batch_size=self.batch_size))
        targ = self.validation_data[-3]
        thres = optimise_f2_global_threshold(targ, predict, resolution=50)
        print(' ---> f2_val = {0:.5f} (thres={1:.3f})'.format(f2_score(targ, predict > thres),thres))
        sys.stdout.flush()
        return


def load_split(x_train,y_train,split,seed_sss,verbose=False):

    print(x_train.shape)
    print(y_train.shape)
    trn_index = []
    val_index = []
    # change split value for getting different validation splits
    print('Computing stratified shuffle splitting')
    index = np.arange(x_train.shape[0])
    for i in tqdm(range(0, 17)):
        sss = StratifiedShuffleSplit(
            n_splits=2, test_size=split, random_state=seed_sss)
        for train_index, test_index in sss.split(index, y_train[:, i]):
            X_train, X_test = index[train_index], index[test_index]
        # to ensure there is no repetetion within each split and between the splits
        trn_index = trn_index + \
            list(set(list(X_train)) - set(trn_index) - set(val_index))
        val_index = val_index + \
            list(set(list(X_test)) - set(val_index) - set(trn_index))

    X_train = x_train[trn_index]
    X_valid = x_train[val_index]
    Y_train = y_train[trn_index]
    Y_valid = y_train[val_index]

    if(verbose):
        print('_'*100)
        print('x_train shape = ', X_train.shape)
        print('y_train shape = ', Y_train.shape)
        print('x_valid shape = ', X_valid.shape)
        print('y_valid shape = ', Y_valid.shape)
        print('Split train   = ', len(X_train), len(Y_train))
        print('Split valid   = ', len(X_valid), len(Y_valid))

    return X_train, X_valid, Y_train, Y_valid


warnings.simplefilter('ignore')

model_names = [
    'npix64_jpg_augmented3.h5',
    'npix64_jpg_augmented3_model2.h5',
    'npix64_jpg_augmented3_model3.h5',
    'npix64_jpg_augmented3_model4.h5',
    'npix64_jpg_augmented3_model5.h5',
    'npix64_jpg_augmented3_unet1.h5',
    'npix128_jpg_augmented3.h5',
    'npix128_jpg_augmented3_model3.h5',
    'npix128_jpg_augmented3_unet1.h5'
]
input_names = [
    'input/train_px64_jpg_augmented3.h5',
    'input/train_px64_jpg_augmented3.h5',
    'input/train_px64_jpg_augmented3.h5',
    'input/train_px64_jpg_augmented3.h5',
    'input/train_px64_jpg_augmented3.h5',
    'input/train_px64_jpg_augmented3.h5',
    'input/train_px128_jpg_augmented3.h5',
    'input/train_px128_jpg_augmented3.h5',
    'input/train_px128_jpg_augmented3.h5'
]
test_names = [
    'input/test_px64_jpg_augmented3.h5',
    'input/test_px64_jpg_augmented3.h5',
    'input/test_px64_jpg_augmented3.h5',
    'input/test_px64_jpg_augmented3.h5',
    'input/test_px64_jpg_augmented3.h5',
    'input/test_px64_jpg_augmented3.h5',
    'input/test_px128_jpg_augmented3.h5',
    'input/test_px128_jpg_augmented3.h5',
    'input/test_px128_jpg_augmented3.h5'
]
epochs = [20,20,20,20]
nbatch = 16
std_noise = 0.005
learn_rate = [1e-3,5e-4,2e-4,1e-4]
split = 0.20
split_ens = 0.50
nbatch_ens = 16
seed_sss = 777
drop = 0.25
opt = 'Adam'
model_path = 'models/ensemble.h5'
threshold_filename = 'models/ensemble_thresholds.p'
input_list_filename = 'models/ensemble_inputs.p'
with open(input_list_filename,'wb') as p:
    pickle.dump([model_names,input_names,test_names],p)

np.random.seed(seed_sss)

myopt = operator.attrgetter(opt)(k.optimizers)


models = []
p_valid = []
p_train = []
thres = []

iprev = ''
for m,i in zip(model_names,input_names):
    print('Loading ', m,' input ',i)
    print('_'*100)
    models.append(k.models.load_model('models/' + m))
    if(i!=iprev):
        with h5py.File(i, 'r') as hf:
            X_valid = hf['x_valid'][:]
            Y_valid = hf['y_valid'][:]
        #X_train, X_valid, Y_train, Y_valid = load_split(x_train,y_train,split,seed_sss)
        #del x_train,y_train,X_train
        #gc.collect()
    #p_train.append(models[-1].predict(X_train, verbose=1))
    p_valid.append(models[-1].predict(X_valid, verbose=1))
    print('')
    thres.append(optimise_f2_global_threshold(Y_valid, p_valid[-1], resolution=300))
    #p_train[-1]=(p_train[-1]>thres[-1]).astype(float)
    #p_valid[-1]=(p_valid[-1]>thres[-1]).astype(float)
    print('_'*100)
    print('fbeta  = {0:.4f}'.format(f2_score(Y_valid, p_valid[-1]>thres[-1])))
    print('_'*100)
    iprev = i

#xN_train = np.stack(p_train, axis=2)
xN_valid = np.stack(p_valid, axis=2)
#print('Ensembling train shape: ',xN_train.shape)
print('Ensembling valid shape: ',xN_valid.shape)

_input = k.layers.Input(shape=xN_valid.shape[1:], name='input')
#norm1 = k.layers.BatchNormalization()(_input)
ff = k.layers.noise.GaussianNoise(std_noise)(_input)
ff = k.layers.Flatten()(ff)
ff = k.layers.Dense(17*6,activation='relu')(ff)
ff = k.layers.Dropout(drop)(ff)
ff = k.layers.Dense(17*6,activation='relu')(ff)
ff = k.layers.BatchNormalization()(ff)
ff = k.layers.Dropout(2*drop)(ff)
out1 = k.layers.Dense(4, activation='softmax')(ff)
out2 = k.layers.Dense(13, activation='sigmoid')(ff)
out = k.layers.concatenate(inputs=[out1, out2])

model = k.models.Model(inputs=_input, outputs=out)

opt = myopt(learn_rate)
model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
model.summary()
callbacks = [f2_callback(batch_size=nbatch, verbose=1)]


XN_train, XN_valid, YN_train, YN_valid =  load_split(xN_valid,Y_valid,split_ens,seed_sss)
del xN_valid
gc.collect()

print('XN_train :',XN_train.shape)
print('XN_valid :',XN_valid.shape)
print('Y_valid  :',Y_valid.shape)
print('_'*100)

for ep,lr,i in zip(epochs,learn_rate,range(len(epochs))):
    opt = myopt(lr=lr)
    model.compile(loss='binary_crossentropy',
                  optimizer=opt, metrics=['accuracy'])
    callbacks = [k.callbacks.ModelCheckpoint(
        model_path, monitor='val_loss', save_best_only=True, verbose=0), f2_callback(batch_size=nbatch, verbose=1)]
    if(i > 0):
        model.load_weights(model_path)
    model.fit(x=XN_train, y=YN_train, batch_size=nbatch_ens, verbose=1,
            epochs=ep, callbacks=callbacks, shuffle=True, validation_data=(XN_valid,YN_valid))

print('')
PN_valid = model.predict(XN_valid, verbose=1)
print('')
tg = optimise_f2_global_threshold(YN_valid, PN_valid, resolution=500)
print('fbeta ensemble  = {0:.4f}'.format(f2_score(YN_valid, PN_valid>tg)))

with open(threshold_filename,'wb') as p:
    pickle.dump(tg,p)
